import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Colors from "../Constant/Colors";
import Login from "../Screens/Login";
const Stack = createNativeStackNavigator();
function MainNavigator() {
    return (
        <Stack.Navigator
            screenOptions={{
                contentStyle: { backgroundColor: Colors.background },
                headerShown: false,
                headerStyle: { backgroundColor: Colors.secondary },
            }}
        >
            <Stack.Screen name="Login" component={Login} />
        </Stack.Navigator>
    );
}
export default MainNavigator;