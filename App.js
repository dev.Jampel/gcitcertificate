import "react-native-get-random-values"; 
import "@ethersproject/shims";

import { StatusBar } from 'expo-status-bar';
import { StyleSheet, SafeAreaView } from 'react-native';
import Colors from "./Constant/Colors";
import MainNavigator from "./Navigators/MainNavigator";
import { Provider } from "react-redux";
import { store } from "./Store/store";


import { NavigationContainer } from "@react-navigation/native";


export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      {/* <Text style={styles.text}>Page content</Text> */}
      <Provider store={store}>
        <NavigationContainer>
          {/* <Login/> */}
          <MainNavigator/>
        </NavigationContainer>
      </Provider>
      <StatusBar style="dark" backgroundColor={Colors.secondary} />
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.secondary,
  },
});
